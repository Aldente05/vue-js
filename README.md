# nearmiss

> Nearmiss ICON+ for Indonesian Power

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
ini diambil dari script package.json
dia eksekusi script dev

npm run dev

# build for production with minification
ini diambil dari script package.json
dia eksekusi script build
npm run build

# build for production and view the bundle analyzer report
ini diambil dari script package.json
dia eksekusi script build

npm run build --report

# run unit tests
ini diambil dari script package.json
dia eksekusi script unit

npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
