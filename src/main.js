// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// set cookie untuk keperuan session management
Vue.prototype.setCookie = (cName, value, expiredays) => {
  const exdate = new Date()
  exdate.setDate(exdate.getDate() + expiredays)
  document.cookie = cName + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString())
}

// get cookie untuk keperuan session management
this.getCookie = (name) => {
  let arr = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
  let reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
  if (arr === document.cookie.match(reg)) {
    return (arr[2])
  } else {
    return null
  }
}
// init cookie dari get
Vue.prototype.getCookie = this.getCookie

// delete cookie
Vue.prototype.delCookie = (name) => {
  const exp = new Date()
  exp.setTime(exp.getTime() - 1)
  const cval = this.getCookie(name)
  if (cval !== null) {

  }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
  watch: {
    '$route': 'checkLogin'
  },
  created () {
    this.checkLogin()
  },
  methods: {
    checkLogin () {
      if (!this.getCookie('session')) {
        this.$router.push('/login')
      } else {
        this.$router.push(router)
      }
    }
  }
})
