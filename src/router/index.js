import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/credentials/Login'
import Dashboard from '@/components/dashboard/Dashboard'

Vue.use(Router)
/**
ini adalah module router untuk setiap component yang ada didalam package
component yang didapat melalu import seperti berikut :

 - import NamaRoute from '@/path/to/component'

 **/
export default new Router({
  routes: [{
    path: '/login',
    component: Login
  }, {
    path: '/dashboard',
    component: Dashboard
  }]
})
